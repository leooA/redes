$dig +trace facebook.com

; <<>> DiG 9.10.3-P4-Ubuntu <<>> +trace facebook.com
;; global options: +cmd
.			331686	IN	NS	h.root-servers.net.
.			331686	IN	NS	m.root-servers.net.
.			331686	IN	NS	c.root-servers.net.
.			331686	IN	NS	l.root-servers.net.
.			331686	IN	NS	g.root-servers.net.
.			331686	IN	NS	k.root-servers.net.
.			331686	IN	NS	f.root-servers.net.
.			331686	IN	NS	b.root-servers.net.
.			331686	IN	NS	i.root-servers.net.
.			331686	IN	NS	d.root-servers.net.
.			331686	IN	NS	a.root-servers.net.
.			331686	IN	NS	e.root-servers.net.
.			331686	IN	NS	j.root-servers.net.
.			504503	IN	RRSIG	NS 8 0 518400 20180307170000 20180222160000 41824 . bRKqmySDpEdw/LjFQ/UDYsoAvpB4XGP/g8qECHegBotut/7zgi2VuXYm SrPyScsi/FsTlbAtve07/oF5WpSryRHaEyHoJkaDayDGBohjojenH11w QSutanNSdb2DZOvUekNMwt4cCMvVROgpzVZ1k4OGMWz/xSz9/9c1OlS3 Yd3U9sGkxmIiiOyk5xH0v0+GuU7UOUjP/82JE6S85+DQ8hCVk0pJJ7SR rHx7bjsn9Uxi59agboA0nGQFmgT2urqwESIJ8h8Bwl6j/pBOtyMb2pIj iiDpy1WnT2Q+MSWfopgCDibVp8ip31aaXuELbwcC3oL9BDAKt8TLVJtx kqy+/g==
;; Received 1097 bytes from 127.0.1.1#53(127.0.1.1) in 6 ms

com.			172800	IN	NS	k.gtld-servers.net.
com.			172800	IN	NS	m.gtld-servers.net.
com.			172800	IN	NS	a.gtld-servers.net.
com.			172800	IN	NS	d.gtld-servers.net.
com.			172800	IN	NS	j.gtld-servers.net.
com.			172800	IN	NS	f.gtld-servers.net.
com.			172800	IN	NS	b.gtld-servers.net.
com.			172800	IN	NS	c.gtld-servers.net.
com.			172800	IN	NS	e.gtld-servers.net.
com.			172800	IN	NS	h.gtld-servers.net.
com.			172800	IN	NS	g.gtld-servers.net.
com.			172800	IN	NS	i.gtld-servers.net.
com.			172800	IN	NS	l.gtld-servers.net.
com.			86400	IN	DS	30909 8 2 E2D3C916F6DEEAC73294E8268FB5885044A833FC5459588F4A9184CF C41A5766
com.			86400	IN	RRSIG	DS 8 1 86400 20180307190000 20180222180000 41824 . H6MljsznuAQnkvk/UiFo7QTs9u+4C6CIS081Q1pkiiZ5RM5om1Ae6fqo oub27bhDZfwRhZ/2HKNZEcOrsLdwNqYjuP9j5D3850pZaV6u5IARJ72W Tqf+3Q+HycbkQvh56w199+aExFf7xemslU6Rf9Ha12llt8dmVdgezN5V /ukGmbkT6ZI5akpohcqMEhUildQfBcrfO0gIcm98J1v6bulJIhQf5oUW IVIM0agnFZMbk/uaf63xWVHvWk34N7kjG5zJsNf6mE5/JSW4/c1lMsui RnnS1UbDTdDVjWr7MUf/XS+KpWyB/MWoDYSP/H0/w/6peHywLJI/ldTW VS8klw==
;; Received 1172 bytes from 192.112.36.4#53(g.root-servers.net) in 82 ms

facebook.com.		172800	IN	NS	a.ns.facebook.com.
facebook.com.		172800	IN	NS	b.ns.facebook.com.
CK0POJMG874LJREF7EFN8430QVIT8BSM.com. 86400 IN NSEC3 1 1 0 - CK0Q1GIN43N1ARRC9OSM6QPQR81H5M9A NS SOA RRSIG DNSKEY NSEC3PARAM
CK0POJMG874LJREF7EFN8430QVIT8BSM.com. 86400 IN RRSIG NSEC3 8 2 86400 20180301054933 20180222043933 46967 com. PIwxI0liYl0qed0OMhFCruscH1IPW3eOQBeTGUrd09FZSYrEYBeombR7 9fujdHvFk8t9ZfZcFfQsWwCu5XrKcpXe+kvEIfYffguN+26pG/HW6h+T Dr4Y7tYlHmWNM0fG/oz9nnAJ+96pFu/eugp6gyK3otXvL2Xu8I2/Di8p Wrc=
I28F95VQ7BMV0A9LL91T0Q6FQJ9J37A2.com. 86400 IN NSEC3 1 1 0 - I28HHB63CQA80ATFKKTPK9B6NJPVGCI5 NS DS RRSIG
I28F95VQ7BMV0A9LL91T0Q6FQJ9J37A2.com. 86400 IN RRSIG NSEC3 8 2 86400 20180228055548 20180221044548 46967 com. UdL5UDxIKa1VS1CBEGdCHWi/yQUZWApbnm5K1uexnn4PeK8XwtHKrz7P 536sYyYi4irC4JA4WL1msD9j+t96xJPYWF47AknaLLA6vM79nsxaB2PV B1epMyexK9XM04o5MOoDAfCL/RK/DtsUmkh7PDqVgo0gUTUDDhD1j/TU jdM=
;; Received 649 bytes from 192.5.6.30#53(a.gtld-servers.net) in 87 ms

facebook.com.		300	IN	A	157.240.3.35
facebook.com.		172800	IN	NS	a.ns.facebook.com.
facebook.com.		172800	IN	NS	b.ns.facebook.com.
;; Received 180 bytes from 69.171.255.12#53(b.ns.facebook.com) in 64 ms
